/*
 * nukibridge.hpp
 *
 *  Created on: Aug 29, 2018
 *      Author: vlad
 */
#pragma once
#include "makeintrusive.hpp"
#include "p44vdc_common.hpp"
#include "jsonobject.hpp"
#include "jsonwebclient.hpp"
#include "boost/optional.hpp"
#include "boost/signals2.hpp"
#include "statuscallback.hpp"
#include "nukiibridge.hpp"
#include "ipersistentstoragefactory.hpp"


namespace p44 {

namespace nuki {


  class Bridge : public IBridge
  {
  public:

    Bridge(JsonClient & aJsonClient, const string& aIp, const string& aMacAddress,
        const string& aSslCAFile, IPersistentStorageFactory& aStorageFactory, const string& aDsUID);
    ~Bridge() override {}
    void authorize(StatusCB aStatusCB) override;
    string getIp() const override;
    void setToken(const string& aToken) override;
    string getToken() const override;
    void getLocks(GetLocksCB aNukiLocksCB) override;
    JsonClient& getJsonClient() override { return jsonClient; }
    bool getCallbackState() override { return callbackOK; }
    boost::optional<string> buildRequest(BridgeRequest aRequest, string aCallbackId = CALLBACK_ID) const override;



    virtual boost::signals2::connection registerCallback(UpdateDataCB aCallback);

    using StatusUpdateCB = Callback<void(AccountStatus aStatus)>;

  private:
    void pollCycle();
    void cancelPollCycle();

    string dsUID;
    string ip;
    string accessToken;
    JsonClient & jsonClient;
    bool callbackOK = false;

    MLTicket pollDataTicket;
    boost::signals2::signal<void(JsonObjectPtr)> dataPollCBs;

    // basing on api description:
    // "Do not try to pull data every minute.
    // Netatmo Weather Station sends its measures to the server every ten minutes"
    static const MLMicroSeconds POLLING_INTERVAL = (10*Minute);
    static const int REFRESH_TOKEN_RETRY_MAX = 3;

    StatusUpdateCB statusUpdatedCB;

    std::unique_ptr<IPersistentStorageWithRowId> storage;

  protected:
    void listCallback(GetCallbacksCB aCallbackListCB);
    void removeCallback(const string& id);
    void checkCallbackList(StatusCB aStatusCB);
    void configureCallback(StatusCB aStatusCB);
    void registerLockCallback(StatusCB aStatusCB);
    void setCallbackFlag(ErrorPtr aErrorPtr, StatusCB aStatusCB);
  };

} // namespace nuki
} // namespace p44
