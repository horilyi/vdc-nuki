/*
 * nukioperation.cpp
 *
 *  Created on: Aug 23, 2018
 *      Author: vlad
 */

// File scope debugging options
// - Set ALWAYS_DEBUG to 1 to enable DBGLOG output even in non-DEBUG builds of this file
#define ALWAYS_DEBUG 0
// - set FOCUSLOGLEVEL to non-zero log level (usually, 5,6, or 7==LOG_DEBUG) to get focus (extensive logging) for this file
//   Note: must be before including "logger.hpp" (or anything that includes "logger.hpp")
#define FOCUSLOGLEVEL 7

#include "nukioperation.hpp"


using namespace p44::nuki;

Operation::Operation(JsonClient& aJsonClient, const string& aUrlPath, JsonWebClientCB aCallback) :
    inherited(aJsonClient, HttpMethod::GET, aUrlPath, {}, aCallback) {}

void Operation::sendRequest()
{
  issueRequest();
}


