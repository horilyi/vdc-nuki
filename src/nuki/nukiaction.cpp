/*
 * nukiaction.cpp
 *
 *  Created on: Aug 21, 2018
 *      Author: vlad
 *
 *
 *  This file is part of vdc-nuki
 */

// File scope debugging options
// - Set ALWAYS_DEBUG to 1 to enable DBGLOG output even in non-DEBUG builds of this file
#define ALWAYS_DEBUG 0
// - set FOCUSLOGLEVEL to non-zero log level (usually, 5,6, or 7==LOG_DEBUG) to get focus (extensive logging) for this file
//   Note: must be before including "logger.hpp" (or anything that includes "logger.hpp")
#define FOCUSLOGLEVEL 7


#include "nukiaction.hpp"

using namespace p44::nuki;


Action::Action(Device &aDevice, const string& aId, const string& aDescription, LockAction aLockAction) :
  inherited(aDevice, aId, aDescription),
  action(aLockAction),
  device(aDevice)
  {}

void Action::performCall(ApiValuePtr aParams, StatusCB aCompletedCB)
{
  device.lockAction(action, aCompletedCB);
}
