/*
 * nukidevice.hpp
 *
 *  Created on: Aug 10, 2018
 *      Author: vlad
 *
 *  This file is part of vdc-nuki
 */
#pragma once
#include "p44vdc_common.hpp"

#include "simplescene.hpp"
#include "dsscene.hpp"
#include "singledevice.hpp"
#include "binaryinputbehaviour.hpp"
#include "buttonbehaviour.hpp"
#include "typeddevicestate.hpp"
#include "nukivdc.hpp"
#include "nukiibridge.hpp"
#include "boost/optional.hpp"
#include "boost/intrusive_ptr.hpp"
#include "boost/signals2/connection.hpp"

namespace p44 {

namespace nuki {
  enum class StaLockState {
    Uncalibrated,
    Locked,
    Unlocked,
    Unlatched,
    LockAndGo,
    MotorBlocked,
    Undefined
  };

  enum class LockAction {
    Unlock,
    Lock,
    Unlatch,
    LockAndGo,
    LockAndGoWithUnlatch
  };

  enum class DeviceRequest {
    LockAction,
    LockState,
    Unpair
  };

  enum class Event {
    EvLocked,
    EvUnlocked,
    EvErrorState
  };

  class DeviceSettings : public CmdSceneDeviceSettings
  {
    using inherited = CmdSceneDeviceSettings;

  public:

    explicit DeviceSettings(p44::Device &aDevice) :
      inherited(aDevice) {};

    virtual DsScenePtr newDefaultScene(SceneNo aSceneNo);
  };

  class Scene : public SimpleCmdScene
  {
    using inherited = SimpleCmdScene;

    void setAction(const string& aActionName);
  public:

    Scene(DeviceSettings &aSceneDeviceSettings, SceneNo aSceneNo) :
      inherited(aSceneDeviceSettings, aSceneNo)  {};

    virtual void setDefaultSceneValues(SceneNo aSceneNo);
  };
  using ScenePtr = boost::intrusive_ptr<Scene>;

  class Device : public SingleDevice
  {
    public:
      Device(p44::Vdc *aVdcP, const string& aNukiName, const string& aNukiId, boost::intrusive_ptr<IBridge> aBridgePtr);

      virtual ~Device();

      virtual bool identifyDevice(IdentifyDeviceCB aIdentifyCB) P44_OVERRIDE;

      virtual string deviceTypeIdentifier() const P44_OVERRIDE { return "nuki"; };

      virtual string description() P44_OVERRIDE;

      virtual void initializeDevice(StatusCB aCompletedCB, bool aFactoryReset) P44_OVERRIDE;

      virtual string hardwareGUID() P44_OVERRIDE;

      virtual string hardwareModelGUID() P44_OVERRIDE;

      virtual string hardwareVersion() P44_OVERRIDE;

      virtual string modelName() P44_OVERRIDE;

      virtual string vendorName() P44_OVERRIDE;

      virtual string oemModelGUID() P44_OVERRIDE;

      virtual ErrorPtr handleMethod(VdcApiRequestPtr aRequest, const string &aMethod, ApiValuePtr aParams) P44_OVERRIDE;

      void setNukiName(const string & name){ this->name = name; }

      string getNukiName() const { return this->name; }

      void setNukiId(const string & nukiId){ this->nukiId = nukiId; }

      string getNukiId() const { return this->nukiId; }

      void lockAction(LockAction action, StatusCB aCompletedCB);
      void getLockState();
      boost::optional<string> buildRequest(DeviceRequest aRequest, boost::optional<string> action = boost::none);

      // method for handling event server responses (from signals send by server)
      void callbackDataHandler(JsonObjectPtr& aResponse, p44::ErrorPtr aError);

      void setConnctionForSlot(boost::signals2::connection aConnection){
        this->connection = aConnection;
      }
      boost::intrusive_ptr<IBridge> bridgePtr;

    private:
      using inherited = SingleDevice;

      void configureDevice();
      void deriveDsUid();
      boost::optional<string> getActionId(LockAction lockAction);
      boost::optional<string> mapEventToString(Event aEvent);
      boost::optional<StaLockState> mapStringToStaLockState(const string & aStaLockState);
      p44::DeviceEventPtr mapEvnetToState(const StaLockState & aStaLockState);

      boost::intrusive_ptr<p44::TypedDeviceState<StaLockState>> staLockState;

      string name;
      string nukiId;
      boost::signals2::connection connection;
  }; 

} // namespace nuki
} // namespace p44
